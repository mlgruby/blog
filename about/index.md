---
layout: page
title: About me
tags: [about, Satyasheel]
modified: 2015-12-04T20:53:07.573882-04:00
comments: false
paragraph: true
share: false
---

Hi there! I am a graduate student at [Royal Holloway University of London](https://www.royalholloway.ac.uk/home.aspx), pursuing MSc Machine Learning. Prior to this I was Data Scientist working at [Cyient-Insights](http://www.cyient-insights.com/) an analytics devision of [Cyient](http://www.cyient.com) in [M2M](http://en.wikipedia.org/wiki/Machine_to_machine) domain. Here I validated, developed different statistical models and also build prediction models for maintenance system. Prior to Cyient-Insights I was working as System Engineer at [Infosys](http://www.infosys.com) where I developed web application facilitating "Enterprise Lending" system for leading banking client.

My interest lies in Machine Learning perticularly in "[Estimation Theory](http://en.wikipedia.org/wiki/Estimation_theory)" and "[Mathematical Optimazation](http://en.wikipedia.org/wiki/Mathematical_optimization)". Apart from core interest I regularly spend time on [Kaggle](http://www.kaggle.com/users/182203/satyasheel) (Level KAGGLER) for different predictions challenges and also a Coursera learner, completed [Data Science specialization](https://www.coursera.org/specialization/jhudatascience/1) course offered by Johns Hopkins University. Second to data science specialization course I am also a certified [Big Data Analytics and Optimization](https://dl.dropboxusercontent.com/u/16579133/287_Satyasheel.pdf) from [INSOFE](http://insofe.edu.in/) and verified by [lti](https://www.lti.cs.cmu.edu/) of Carnegie Mellon University. This certification ranks third in big data certifications by [CIO](http://www.cio.com/article/2457266/certifications/160612-11-Big-Data-Certifications-That-Will-Pay-Off.html). All my certification and projects can be found on [Linkedin](https://www.linkedin.com/in/ss6012) page.

DIY enthisiast by nature and strategies games palyer, preferably DOTA2 and AOEII on weekends plus love to click monocrome picture.

### Contact

I am always eager to talk with other people who share an interest in machine learning; feel free to contact me.

<a markdown="0" href="mailto:satyasheel@ymail.com" class="btn btn-info">Contact Me</a>