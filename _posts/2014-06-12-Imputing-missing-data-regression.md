---
layout: post
title: "Imputing missing data using Linear Regression model"
excerpt: Imputing missing data in a data frame by Linear Regression method (Demo is in R)
modified: 2014-09-12
comments: true
share: true
---
   Missing data is a widely recognized problem affecting large databases that creates problems in many applications that depend on access to complete data records such as data visualization and reporting tools. This problem also limits data analysts interested in making policy decisions based on statistical inference from the data and thus imputing miss ing data is often invaluable as it preserves information and produces better, less biased estimates than simple techniques such as list-wise deletion and mean-value substitution.
  
  The missing data problem is common and often unavoidable especially when dealing with large data sets from several real-world sources.

#### The Missing Values Problem
  Luengo, J. introduced three problems associated with missing values as:- 
   
- loss of efficiency,
- complications in handling and analyzing the data,
- bias resulting from differences between missing and complete data.
 

While working on several dataset I faced the same problem. So to get out this problems what techniques should one use. There are several papers out on web which explains the different techniques, but for the dataset which I was working I followed the "Linear Regression" model to impute the missing data. And this some how increases my prediction accuracy.

**Note:-**


- Linear Regression is not always used for imputing missing data. First check for correlation between columns then proceed with this model.

Consider a data frame name ```BankingClientData``` (Contains banking information for all the customers). Let us assume that a pair of column are highly correlated say correlation between them is 0.94 (through this we can say that one is linearly dependent on other column)

Let's say two column which highly correlated are ```salary``` and ```loan``` and lets say ```loan``` has missing data.

In R one can easily build a linear regression model by ```lm(y~x)``` function where ```y``` is predicted value and ```x``` is predictor. Mathematically :-

$$
y = \beta_1x + \beta_2
$$

where $$ \beta1 $$ and $$ \beta2 $$ are coefficients (or one can say  that $$ \beta1 $$ is slope and $$ \beta2 $$ is intercept).

{% highlight R %}
lrModel <- lm(BankingClientData$loan~BankingClientData$salary, 
		   data = BankingClientData)
{% endhighlight %}

$$
loan = \beta_1salary + \beta_2
$$

with following code, coefficient($$ \beta1 $$ and $$ \beta2 $$) for ```lrModel``` can be easily checked.
{% highlight R %}
lrModel$coefficients    #this will display coefficinets
{% endhighlight %}

now a function should be made which takes predictor as an input and returns predicted value for missing data.

{% highlight R %}
fillNA <- function(x){
    if(is.na(x)) return(NA)        #return NA for NA predictor
    else return(lrModel$coefficients[[1]] + lrModel$coefficients[[2]] * x)
}
{% endhighlight %}

the only work left is replacing the missing data with predicted data.

{% highlight R %}
#this will iterate over salary data corresponding to NA data in loan column 
#and uses fillNA function to predict data
localLoan <- sapply(sapply(BankingClientData[is.na(BankingClientData$loan), 
			 "salary"], fillNA))
# this will replace NA values in loan column with predicted data in localLoan
BankingClientData[is.na(BankingClientData$loan), "loan"] <- localLoan 
{% endhighlight %}

This post is on how we can use Linear Regression Model to impute missing data. Will post more model based imputation methods for missing data.

references :

1. <a href="http://www.si-journal.org/index.php/JSI/article/viewFile/178/134" target="_blank">Dealing with Missing Values in Data</a>
2. <a href="http://ww2.coastal.edu/kingw/statistics/R-tutorials/simplelinear.html" target="_blank">SIMPLE LINEAR CORRELATION AND REGRESSION</a>
<br>
<br>